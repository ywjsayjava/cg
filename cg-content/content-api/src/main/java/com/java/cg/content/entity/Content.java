package com.java.cg.content.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-14 15:00:11
 */
@Data
@TableName("tb_content")
public class Content implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 内容类目ID
	 */
	private Long categoryId;
	/**
	 * 内容标题
	 */
	private String title;
	/**
	 * 链接
	 */
	private String url;
	/**
	 * 图片绝对路径
	 */
	private String pic;
	/**
	 * 状态,0无效，1有效
	 */
	private String status;
	/**
	 * 排序
	 */
	private Integer sortOrder;

}
