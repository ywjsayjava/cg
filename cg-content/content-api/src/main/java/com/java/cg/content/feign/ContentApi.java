package com.java.cg.content.feign;

import com.java.cg.content.entity.Content;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author jiangli
 * @since 2020/2/14 15:07
 */
@FeignClient("cg-content")
public interface ContentApi {

	/***
	 * 根据categoryId查询广告集合
	 */
	@GetMapping(value = "content/list/category/{id}")
	List<Content> findByCategory(@PathVariable("id") Long id);
}
