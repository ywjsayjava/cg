package com.java.cg.content.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageUtils;
import com.java.cg.content.entity.Content;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author jiangli
 * @since 2020-02-14 15:00:11
 */
public interface ContentService extends IService<Content> {

    PageUtils queryPage(Map<String, Object> params);

	/***
	 * 根据categoryId查询广告集合
	 * @param id
	 * @return
	 */
	List<Content> findByCategory(Long id);
}

