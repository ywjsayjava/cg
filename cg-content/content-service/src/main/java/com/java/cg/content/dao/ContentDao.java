package com.java.cg.content.dao;

import com.java.cg.content.entity.Content;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-14 15:00:11
 */
@Mapper
public interface ContentDao extends BaseMapper<Content> {
	
}
