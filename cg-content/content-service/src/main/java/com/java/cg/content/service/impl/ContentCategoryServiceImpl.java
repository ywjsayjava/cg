package com.java.cg.content.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.content.dao.ContentCategoryDao;
import com.java.cg.content.entity.ContentCategory;
import com.java.cg.content.service.ContentCategoryService;

@Service("contentCategoryService")
public class ContentCategoryServiceImpl extends ServiceImpl<ContentCategoryDao, ContentCategory> implements ContentCategoryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ContentCategory> page = this.page(
                new Query<ContentCategory>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}