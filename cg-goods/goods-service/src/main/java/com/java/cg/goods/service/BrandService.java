package com.java.cg.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageUtils;
import com.java.cg.goods.entity.Brand;

import java.util.List;
import java.util.Map;

/**
 * 品牌表
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
public interface BrandService extends IService<Brand> {

    PageUtils queryPage(Map<String, Object> params);

	List<Brand> findByCategoryId(Integer categoryId);
}

