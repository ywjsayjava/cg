package com.java.cg.goods.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.goods.dao.AlbumDao;
import com.java.cg.goods.entity.Album;
import com.java.cg.goods.service.AlbumService;

@Service("albumService")
public class AlbumServiceImpl extends ServiceImpl<AlbumDao, Album> implements AlbumService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Album> page = this.page(
                new Query<Album>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}