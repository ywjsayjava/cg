package com.java.cg.goods.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.java.cg.goods.entity.Category;
import com.java.cg.goods.service.CategoryService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 商品类目
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@RestController
@RequestMapping("goods/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("{parentId}")
    public R findByPid(@PathVariable("parentId") Integer parentId){
	    List<Category> categories = categoryService.findByPid(parentId);
	    return R.ok(categories);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = categoryService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public Category info(@PathVariable("id") Integer id){
	    return categoryService.getById(id);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Category category){
		categoryService.save(category);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody Category category){
		categoryService.updateById(category);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
		categoryService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
