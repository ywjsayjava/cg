package com.java.cg.goods.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.goods.dao.StockBackDao;
import com.java.cg.goods.entity.StockBack;
import com.java.cg.goods.service.StockBackService;

@Service("stockBackService")
public class StockBackServiceImpl extends ServiceImpl<StockBackDao, StockBack> implements StockBackService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StockBack> page = this.page(
                new Query<StockBack>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}