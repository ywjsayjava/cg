package com.java.cg.goods.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.java.cg.goods.entity.Para;
import com.java.cg.goods.service.ParaService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@RestController
@RequestMapping("goods/para")
public class ParaController {
    @Autowired
    private ParaService paraService;

	@GetMapping("category/{categoryId}")
	public R findByCategoryId(@PathVariable("categoryId") Integer categoryId) {
		List<Para> paras = paraService.findByCategoryId(categoryId);

		return R.ok(paras);
	}

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = paraService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
		Para para = paraService.getById(id);

        return R.ok().put("para", para);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Para para){
		paraService.save(para);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody Para para){
		paraService.updateById(para);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
		paraService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
