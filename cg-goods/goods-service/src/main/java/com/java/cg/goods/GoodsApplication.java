package com.java.cg.goods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author jiangli
 * @since 2020/2/12 22:58
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.java.cg.goods","com.java.common"})
public class GoodsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoodsApplication.class,args);
	}
}
