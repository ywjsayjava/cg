package com.java.cg.goods.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.goods.dao.CategoryBrandDao;
import com.java.cg.goods.entity.CategoryBrand;
import com.java.cg.goods.service.CategoryBrandService;

@Service("categoryBrandService")
public class CategoryBrandServiceImpl extends ServiceImpl<CategoryBrandDao, CategoryBrand> implements CategoryBrandService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryBrand> page = this.page(
                new Query<CategoryBrand>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}