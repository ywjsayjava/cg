package com.java.cg.goods.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.goods.dao.BrandDao;
import com.java.cg.goods.entity.Brand;
import com.java.cg.goods.service.BrandService;

@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, Brand> implements BrandService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Brand> page = this.page(
                new Query<Brand>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

	@Override
	public List<Brand> findByCategoryId(Integer categoryId) {
		return this.baseMapper.findByCategoryId(categoryId);
	}

}