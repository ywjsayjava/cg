package com.java.cg.goods.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.java.cg.goods.entity.Brand;
import com.java.cg.goods.service.BrandService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 品牌表
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@RestController
@RequestMapping("goods/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

	/**
	 * 根据分类id查询品牌
	 */
	@GetMapping("category/{categoryId}")
	public R findByCategoryId(@PathVariable("categoryId") Integer categoryId) {
	    List<Brand> brands = brandService.findByCategoryId(categoryId);
	    return R.ok(brands);
    }

    /**
     * 列表
     */
    @GetMapping
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = brandService.queryPage(params);

        return R.ok().put("data", page);
    }


    /**
     * 信息
     */
    @GetMapping("{id}")
    public R info(@PathVariable("id") Integer id){
		Brand brand = brandService.getById(id);

        return R.ok().put("brand", brand);
    }

    /**
     * 保存
     */
    @PostMapping
    public R save(@RequestBody Brand brand){
		brandService.save(brand);

        return R.ok();
    }

    /**
     * 修改
     */
    @PutMapping
    public R update(@RequestBody Brand brand){
		brandService.updateById(brand);

        return R.ok();
    }

    /**
     * 删除
     */
    @DeleteMapping
    public R delete(@RequestBody Integer[] ids){
		brandService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
