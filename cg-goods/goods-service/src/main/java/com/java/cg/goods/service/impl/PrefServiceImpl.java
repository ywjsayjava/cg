package com.java.cg.goods.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.goods.dao.PrefDao;
import com.java.cg.goods.entity.Pref;
import com.java.cg.goods.service.PrefService;

@Service("prefService")
public class PrefServiceImpl extends ServiceImpl<PrefDao, Pref> implements PrefService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Pref> page = this.page(
                new Query<Pref>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}