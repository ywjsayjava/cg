package com.java.cg.goods.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.cg.goods.entity.Sku;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 商品表
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@Mapper
public interface SkuDao extends BaseMapper<Sku> {

	/**
	 * 商品下订单之后更改库存数量
	 */
	@Update("update tb_sku set num = num-#{num} where id = #{id} and num > #{num}")
	int decrCount(@Param("id") Long id, @Param("num") Integer num);

}
