package com.java.cg.goods.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.goods.dao.SpecDao;
import com.java.cg.goods.entity.Spec;
import com.java.cg.goods.service.SpecService;

@Service("specService")
public class SpecServiceImpl extends ServiceImpl<SpecDao, Spec> implements SpecService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Spec> page = this.page(
                new Query<Spec>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

	/**
	 * categoryId --> templateId --> spec
	 */
	@Override
	public List<Spec> findByCategoryId(Integer categoryId) {

		return this.baseMapper.findByCategoryId(categoryId);
	}

}