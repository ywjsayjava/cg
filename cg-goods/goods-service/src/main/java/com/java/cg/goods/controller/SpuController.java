package com.java.cg.goods.controller;

import java.util.Arrays;
import java.util.Map;

import com.java.cg.goods.entity.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.java.cg.goods.entity.Spu;
import com.java.cg.goods.service.SpuService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@RestController
@RequestMapping("goods/spu")
public class SpuController {
    @Autowired
    private SpuService spuService;

	/**
	 * 根据spuId查询上架商品
	 */
	@PutMapping("put/{spuId}")
	public R put(@PathVariable("spuId") Integer spuId) {
		spuService.put(spuId);

		return R.ok();
	}

	/**
	 * 根据spuId查询下架商品
	 */
	@PutMapping("pull/{spuId}")
	public R pull(@PathVariable("spuId") Integer spuId) {
		spuService.pull(spuId);

		return R.ok();
	}

	/**
	 * 根据spuId审核商品
	 */
	@PutMapping("audit/{spuId}")
	public R audit(@PathVariable("spuId") Integer spuId) {
		spuService.audit(spuId);

		return R.ok();
	}

	/**
	 * 根据spuId查询goods
	 */
	@GetMapping("goods/{spuId}")
	public R findGoodsBySpuId(@PathVariable("spuId") Integer spuId) {
		Goods goods = spuService.findGoodsBySpuId(spuId);
		return R.ok(goods);
	}

	/**
	 * 新增商品
	 */
	@PostMapping("save")
	public R saveGoods(@RequestBody Goods goods) {
		spuService.saveGoods(goods);

		return R.ok();
	}

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = spuService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{spuId}")
    public Spu info(@PathVariable("spuId") Long spuId){
	    return spuService.getById(spuId);

    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Spu spu){
		spuService.save(spu);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody Spu spu){
		spuService.updateById(spu);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		spuService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
