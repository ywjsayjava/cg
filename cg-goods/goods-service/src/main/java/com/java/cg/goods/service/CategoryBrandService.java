package com.java.cg.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageUtils;
import com.java.cg.goods.entity.CategoryBrand;

import java.util.Map;

/**
 * 
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
public interface CategoryBrandService extends IService<CategoryBrand> {

    PageUtils queryPage(Map<String, Object> params);
}

