package com.java.cg.goods.controller;

import java.util.Arrays;
import java.util.Map;

import com.java.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.goods.entity.Album;
import com.java.cg.goods.service.AlbumService;
import com.java.common.utils.PageUtils;



/**
 * 
 *
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@RestController
@RequestMapping("goods/album")
public class AlbumController {
    @Autowired
    private AlbumService albumService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = albumService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		Album album = albumService.getById(id);

        return R.ok().put("album", album);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Album album){
		albumService.save(album);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody Album album){
		albumService.updateById(album);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		albumService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
