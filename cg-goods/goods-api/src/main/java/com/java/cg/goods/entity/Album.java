package com.java.cg.goods.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@Data
@TableName("tb_album")
public class Album implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 编号
	 */
	@TableId
	private Long id;
	/**
	 * 相册名称
	 */
	private String title;
	/**
	 * 相册封面
	 */
	private String image;
	/**
	 * 图片列表
	 */
	private String imageItems;

}
