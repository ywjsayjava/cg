package com.java.cg.goods.feign;

import com.java.cg.goods.entity.Category;
import com.java.cg.goods.entity.Sku;
import com.java.cg.goods.entity.Spu;
import com.java.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author jiangli
 * @since 2020/2/14 16:42
 */
@FeignClient("cg-goods")
public interface GoodsApi {

	@RequestMapping("goods/sku/search/{page}/{size}")
	List<Sku> search(@RequestParam("page") Integer page, @RequestParam("size") Integer size, @RequestBody(required = false) Sku sku);

	/**
	 * 根据分类id查询分类信息
	 */
	@GetMapping("goods/category/info/{id}")
	Category findCategoryById(@PathVariable("id") Integer id);

	/**
	 * 根据spuId查询spu
	 */
	@GetMapping("goods/spu/info/{spuId}")
	Spu findSpuById(@PathVariable("spuId") Long spuId);

	/**
	 * 根据spuId查询skus
	 */
	@GetMapping("goods/sku/list/{spuId}")
	List<Sku> getSkusBySpuId(@PathVariable("spuId") Long spuId);

	/**
	 * 根据skuId查sku
	 */
	@GetMapping("goods/sku/info/{id}")
	Sku getSkuInfoBySkuId(@PathVariable("id") Long id);


	/**
	 * 减库存
	 */
	@GetMapping("goods/sku/decr")
	R decr(@RequestParam("decrMap") Map<String, Integer> decrMap);
}
