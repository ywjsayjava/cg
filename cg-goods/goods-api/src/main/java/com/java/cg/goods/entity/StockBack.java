package com.java.cg.goods.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@Data
@TableName("tb_stock_back")
public class StockBack implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 订单id
	 */
	@TableId
	private String orderId;
	/**
	 * SKU的id
	 */
	private String skuId;
	/**
	 * 回滚数量
	 */
	private Integer num;
	/**
	 * 回滚状态
	 */
	private String status;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 回滚时间
	 */
	private Date backTime;

}
