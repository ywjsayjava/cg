package com.java.cg.goods.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-12 22:26:33
 */
@Data
@TableName("tb_spec")
public class Spec implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Integer id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 规格选项
	 */
	private String options;
	/**
	 * 排序
	 */
	private Integer seq;
	/**
	 * 模板ID
	 */
	private Integer templateId;

}
