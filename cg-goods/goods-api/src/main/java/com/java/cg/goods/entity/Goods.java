package com.java.cg.goods.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author jiangli
 * @since 2020/2/13 15:44
 */
@Data
public class Goods implements Serializable{

	private Spu spu;

	private List<Sku> skus;
}
