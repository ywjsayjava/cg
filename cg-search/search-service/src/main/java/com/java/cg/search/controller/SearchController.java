package com.java.cg.search.controller;

import com.java.cg.search.service.SearchService;
import com.java.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author jiangli
 * @since 2020/2/14 16:49
 */
@RestController
@RequestMapping(value = "/search")
@CrossOrigin
public class SearchController {
	@Autowired
	private SearchService searchService;

	/**
	 * 导入数据
	 * @return
	 */
	@GetMapping("/import")
	public R search(){
		searchService.importSku();
		return R.ok();
	}

	/**
	 * 搜索
	 */
	@GetMapping
	public Map<String,Object> search(@RequestParam(required = false) Map<String, String> searchMap) {
		return searchService.search(searchMap);
	}
}
