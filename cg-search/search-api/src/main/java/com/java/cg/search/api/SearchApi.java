package com.java.cg.search.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author jiangli
 * @since 2020/2/15 11:12
 */
@FeignClient("cg-search")
public interface SearchApi {
	/**
	 * 搜索
	 */
	@GetMapping("search")
	Map<String,Object> search(@RequestParam(required = false) Map<String, String> searchMap);
}
