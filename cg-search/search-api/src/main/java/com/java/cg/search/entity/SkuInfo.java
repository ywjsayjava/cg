package com.java.cg.search.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @author jiangli
 * @since 2020/2/14 10:41
 * 文档映射Bean
 */
@Data
@Document(indexName = "skuinfo",type = "docs")
public class SkuInfo implements Serializable {
    //商品id，同时也是商品编号
    @Id
    private Long id;

    //SKU名称
	/**
	 * type:字段的类型,FieldType.Text支持分词  FieldType.Keyword不支持分词
	 * index:是否索引,true表示该字段(或者叫域)能够搜索,默认true
	 * store:是否存储,也就是页面上显示,默认false
	 * analyzer:创建索引时使用的分词器
	 * searchAnalyzer:搜索时使用的分词器
	 */
    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String name;

    //商品价格，单位为：元
    @Field(type = FieldType.Double)
    private Long price;

    //库存数量
    private Integer num;

    //商品图片
    private String image;

    //商品状态，1-正常，2-下架，3-删除
    private String status;

    //创建时间
    private Date createTime;

    //更新时间
    private Date updateTime;

    //是否默认
    private String isDefault;

    //SPUID
    private Long spuId;

    //类目ID
    private Long categoryId;

    //类目名称
    @Field(type = FieldType.Keyword)
    private String categoryName;

    //品牌名称
    @Field(type = FieldType.Keyword)
    private String brandName;

    //规格
    private String spec;

    //规格参数
    private Map<String,Object> specMap;

}