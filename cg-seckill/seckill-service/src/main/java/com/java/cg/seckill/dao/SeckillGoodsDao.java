package com.java.cg.seckill.dao;

import com.java.cg.seckill.entity.SeckillGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-20 14:53:48
 */
@Mapper
public interface SeckillGoodsDao extends BaseMapper<SeckillGoods> {

}
