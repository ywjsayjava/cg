package com.java.cg.seckill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author jiangli
 * @since 2020/2/20 14:56
 */
@SpringBootApplication
@EnableFeignClients
@EnableScheduling //开启定时任务
@EnableAsync //开启异步
public class SeckillApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeckillApplication.class,args);
	}
}
