package com.java.cg.seckill.service.impl;

import com.java.common.constants.CacheKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.seckill.dao.SeckillGoodsDao;
import com.java.cg.seckill.entity.SeckillGoods;
import com.java.cg.seckill.service.SeckillGoodsService;

@Service("seckillGoodsService")
public class SeckillGoodsServiceImpl extends ServiceImpl<SeckillGoodsDao, SeckillGoods> implements SeckillGoodsService {
	@Autowired
	private RedisTemplate redisTemplate;

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		IPage<SeckillGoods> page = this.page(
				new Query<SeckillGoods>().getPage(params),
				new QueryWrapper<>()
		);

		return new PageUtils(page);
	}

	@Override
	public List<SeckillGoods> listByTime(String time) {
		return redisTemplate.boundHashOps(CacheKey.SEC_KILL_GOODS_PREFIX + time).values();
	}

	@Override
	public SeckillGoods one(String time, Long id) {
		return (SeckillGoods) redisTemplate.boundHashOps(CacheKey.SEC_KILL_GOODS_PREFIX + time).get(id);
	}

}