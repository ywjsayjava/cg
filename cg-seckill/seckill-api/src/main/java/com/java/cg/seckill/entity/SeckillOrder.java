package com.java.cg.seckill.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-20 14:53:48
 */
@Data
@TableName("tb_seckill_order")
public class SeckillOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 秒杀商品ID
	 */
	private Long seckillId;
	/**
	 * 支付金额
	 */
	private BigDecimal money;
	/**
	 * 用户
	 */
	private String userId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 支付时间
	 */
	private Date payTime;
	/**
	 * 状态，0未支付，1已支付
	 */
	private String status;
	/**
	 * 收货人地址
	 */
	private String receiverAddress;
	/**
	 * 收货人电话
	 */
	private String receiverMobile;
	/**
	 * 收货人
	 */
	private String receiver;
	/**
	 * 交易流水
	 */
	private String transactionId;

}
