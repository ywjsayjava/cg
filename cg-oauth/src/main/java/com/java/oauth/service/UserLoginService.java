package com.java.oauth.service;

import com.java.oauth.util.AuthToken;

import java.io.UnsupportedEncodingException;

public interface UserLoginService {

    /**
     * 认证操作
     */
    AuthToken login(String username, String password);
}
