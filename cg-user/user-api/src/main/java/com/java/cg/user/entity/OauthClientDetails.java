package com.java.cg.user.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-15 21:54:29
 */
@Data
@TableName("oauth_client_details")
public class OauthClientDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 客户端ID，主要用于标识对应的应用
	 */
	@TableId
	private String clientId;
	/**
	 * 
	 */
	private String resourceIds;
	/**
	 * 客户端秘钥，BCryptPasswordEncoder加密算法加密
	 */
	private String clientSecret;
	/**
	 * 对应的范围
	 */
	private String scope;
	/**
	 * 认证模式
	 */
	private String authorizedGrantTypes;
	/**
	 * 认证后重定向地址
	 */
	private String webServerRedirectUri;
	/**
	 * 
	 */
	private String authorities;
	/**
	 * 令牌有效期
	 */
	private Integer accessTokenValidity;
	/**
	 * 令牌刷新周期
	 */
	private Integer refreshTokenValidity;
	/**
	 * 
	 */
	private String additionalInformation;
	/**
	 * 
	 */
	private String autoapprove;

}
