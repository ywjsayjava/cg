package com.java.cg.user.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 省份信息表
 * 
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
@Data
@TableName("tb_provinces")
public class Provinces implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 省份ID
	 */
	@TableId
	private String provinceid;
	/**
	 * 省份名称
	 */
	private String province;

}
