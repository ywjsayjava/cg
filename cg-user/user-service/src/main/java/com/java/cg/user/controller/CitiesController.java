package com.java.cg.user.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.user.entity.Cities;
import com.java.cg.user.service.CitiesService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 行政区域地州市信息表
 *
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
@RestController
@RequestMapping("user/cities")
public class CitiesController {
    @Autowired
    private CitiesService citiesService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = citiesService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{cityid}")
    public R info(@PathVariable("cityid") String cityid){
		Cities cities = citiesService.getById(cityid);

        return R.ok().put("cities", cities);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Cities cities){
		citiesService.save(cities);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody Cities cities){
		citiesService.updateById(cities);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] cityids){
		citiesService.removeByIds(Arrays.asList(cityids));

        return R.ok();
    }

}
