package com.java.cg.user.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.user.dao.OauthClientDetailsDao;
import com.java.cg.user.entity.OauthClientDetails;
import com.java.cg.user.service.OauthClientDetailsService;

@Service("oauthClientDetailsService")
public class OauthClientDetailsServiceImpl extends ServiceImpl<OauthClientDetailsDao, OauthClientDetails> implements OauthClientDetailsService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OauthClientDetails> page = this.page(
                new Query<OauthClientDetails>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}