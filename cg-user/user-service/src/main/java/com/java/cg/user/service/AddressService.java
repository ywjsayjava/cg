package com.java.cg.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageUtils;
import com.java.cg.user.entity.Address;

import java.util.Map;

/**
 * 
 *
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
public interface AddressService extends IService<Address> {

    PageUtils queryPage(Map<String, Object> params);
}

