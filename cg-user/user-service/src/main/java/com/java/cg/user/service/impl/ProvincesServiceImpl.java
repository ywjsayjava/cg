package com.java.cg.user.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.user.dao.ProvincesDao;
import com.java.cg.user.entity.Provinces;
import com.java.cg.user.service.ProvincesService;

@Service("provincesService")
public class ProvincesServiceImpl extends ServiceImpl<ProvincesDao, Provinces> implements ProvincesService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Provinces> page = this.page(
                new Query<Provinces>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}