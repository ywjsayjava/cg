package com.java.cg.user.dao;

import com.java.cg.user.entity.Address;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
@Mapper
public interface AddressDao extends BaseMapper<Address> {
	
}
