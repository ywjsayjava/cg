package com.java.cg.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageUtils;
import com.java.cg.user.entity.Cities;

import java.util.Map;

/**
 * 行政区域地州市信息表
 *
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
public interface CitiesService extends IService<Cities> {

    PageUtils queryPage(Map<String, Object> params);
}

