package com.java.cg.user.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.user.dao.AreasDao;
import com.java.cg.user.entity.Areas;
import com.java.cg.user.service.AreasService;

@Service("areasService")
public class AreasServiceImpl extends ServiceImpl<AreasDao, Areas> implements AreasService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Areas> page = this.page(
                new Query<Areas>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}