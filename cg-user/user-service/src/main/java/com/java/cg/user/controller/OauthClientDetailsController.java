package com.java.cg.user.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.user.entity.OauthClientDetails;
import com.java.cg.user.service.OauthClientDetailsService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 
 *
 * @author jiangli
 * @since 2020-02-15 21:54:29
 */
@RestController
@RequestMapping("user/oauthclientdetails")
public class OauthClientDetailsController {
    @Autowired
    private OauthClientDetailsService oauthClientDetailsService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = oauthClientDetailsService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{clientId}")
    public R info(@PathVariable("clientId") String clientId){
		OauthClientDetails oauthClientDetails = oauthClientDetailsService.getById(clientId);

        return R.ok().put("oauthClientDetails", oauthClientDetails);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody OauthClientDetails oauthClientDetails){
		oauthClientDetailsService.save(oauthClientDetails);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody OauthClientDetails oauthClientDetails){
		oauthClientDetailsService.updateById(oauthClientDetails);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] clientIds){
		oauthClientDetailsService.removeByIds(Arrays.asList(clientIds));

        return R.ok();
    }

}
