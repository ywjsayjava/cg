package com.java.cg.user.dao;

import com.java.cg.user.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 用户表
 *
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
@Mapper
public interface UserDao extends BaseMapper<User> {

	/**
	 * 增加积分
	 */
	@Update(value = "update tb_user set points = points+#{points} where username=#{username}")
	int addPoints(@Param(value = "points") Integer points, @Param(value = "username") String username);

}
