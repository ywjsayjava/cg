package com.java.cg.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageUtils;
import com.java.cg.user.entity.User;

import java.util.Map;

/**
 * 用户表
 *
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
public interface UserService extends IService<User> {

    PageUtils queryPage(Map<String, Object> params);

	void addPoints(String username, Integer points);
}

