package com.java.cg.user.dao;

import com.java.cg.user.entity.Cities;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 行政区域地州市信息表
 * 
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
@Mapper
public interface CitiesDao extends BaseMapper<Cities> {
	
}
