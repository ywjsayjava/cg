package com.java.cg.user.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.user.entity.Areas;
import com.java.cg.user.service.AreasService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 行政区域县区信息表
 *
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
@RestController
@RequestMapping("user/areas")
public class AreasController {
    @Autowired
    private AreasService areasService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = areasService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{areaid}")
    public R info(@PathVariable("areaid") String areaid){
		Areas areas = areasService.getById(areaid);

        return R.ok().put("areas", areas);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Areas areas){
		areasService.save(areas);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody Areas areas){
		areasService.updateById(areas);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] areaids){
		areasService.removeByIds(Arrays.asList(areaids));

        return R.ok();
    }

}
