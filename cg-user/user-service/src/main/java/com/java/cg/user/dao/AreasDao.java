package com.java.cg.user.dao;

import com.java.cg.user.entity.Areas;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 行政区域县区信息表
 * 
 * @author jiangli
 * @since 2020-02-15 21:54:30
 */
@Mapper
public interface AreasDao extends BaseMapper<Areas> {
	
}
