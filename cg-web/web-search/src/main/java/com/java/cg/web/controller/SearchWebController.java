package com.java.cg.web.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.java.cg.search.api.SearchApi;
import com.java.cg.search.entity.SkuInfo;
import com.java.common.vo.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author jiangli
 * @since 2020/2/15 11:16
 */
@Controller
@RequestMapping("search")
public class SearchWebController {
	@Autowired
	private SearchApi searchApi;

	/**
	 * 搜索
	 */
	@GetMapping("list")
	public String search(@RequestParam(required = false) Map<String, String> searchMap, Model model) {
		//替换特殊字符
		replace(searchMap);
		//1.调用cg-search微服务  根据搜索的条件参数 查询 数据
		Map<String, Object> resultMap = searchApi.search(searchMap);
		//2.将数据设置到model中     (模板文件中 根据th:标签数据展示)
		//搜索的结果设置
		model.addAttribute("result", resultMap);
		//3.设置搜索的条件 回显
		model.addAttribute("searchMap", searchMap);

		//4.记住之前的URL
		//拼接url
		String[] urls = url(searchMap);
		model.addAttribute("url", urls[0]);
		model.addAttribute("sortUrl", urls[1]);

		//创建一个分页的对象  可以获取当前页 和总个记录数和显示的页码(以当前页为中心的5个页码)
		Page<SkuInfo> page = new Page<>(
				Long.valueOf(resultMap.get("total").toString()),
				Integer.valueOf(resultMap.get("pageNum").toString()) + 1,
				Integer.valueOf(resultMap.get("pageSize").toString())
		);

		model.addAttribute("page", page);

		return "search";
	}

	/**
	 * 拼接url
	 */
	private String[] url(Map<String, String> searchMap) {
		StringBuilder url = new StringBuilder("/search/list"); //带排序参数
		StringBuilder sortUrl = new StringBuilder("/search/list"); //不带排序参数
		if (CollectionUtil.isNotEmpty(searchMap)) {
			url.append("?");
			sortUrl.append("?");
			for (Map.Entry<String, String> entry : searchMap.entrySet()) {
				String key = entry.getKey();// keywords / brand  / category
				String value = entry.getValue();//华为  / 华为  / 笔记本
				//2个url都跳过分页参数
				if (key.equals("pageNum")) {
					continue;
				}
				url.append(key).append("=").append(value).append("&");

				//sortUrl跳过排序参数
				if (key.equals("order") || key.equals("sort")) {
					continue;
				}
				sortUrl.append(key).append("=").append(value).append("&");
			}

			//去掉多余的&
			if (url.lastIndexOf("&") != -1) {
				url = new StringBuilder(url.substring(0, url.lastIndexOf("&")));
			}
			if (sortUrl.lastIndexOf("&") != -1) {
				sortUrl = new StringBuilder(sortUrl.substring(0, sortUrl.lastIndexOf("&")));
			}

		}
		return new String[]{url.toString(), sortUrl.toString()};
	}

	/**
	 * 替换特殊字符
	 */
	public void replace(Map<String, String> searchMap) {
		if (CollectionUtil.isNotEmpty(searchMap)) {
			for (Map.Entry<String, String> entry : searchMap.entrySet()) {
				if (entry.getKey().startsWith("spec_")) {
					//将规格参数中的+号替换
					entry.setValue(entry.getValue().replace("+", "%2B"));
				}
			}
		}

	}
}
