package com.java.cg.order.dao;

import com.java.cg.order.entity.ReturnOrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-17 19:17:36
 */
@Mapper
public interface ReturnOrderItemDao extends BaseMapper<ReturnOrderItem> {
	
}
