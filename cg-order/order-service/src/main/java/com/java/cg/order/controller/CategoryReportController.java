package com.java.cg.order.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.order.entity.CategoryReport;
import com.java.cg.order.service.CategoryReportService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 
 *
 * @author jiangli
 * @since 2020-02-17 19:17:37
 */
@RestController
@RequestMapping("order/categoryreport")
public class CategoryReportController {
    @Autowired
    private CategoryReportService categoryReportService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = categoryReportService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{categoryId1}")
    public R info(@PathVariable("categoryId1") Integer categoryId1){
		CategoryReport categoryReport = categoryReportService.getById(categoryId1);

        return R.ok().put("categoryReport", categoryReport);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody CategoryReport categoryReport){
		categoryReportService.save(categoryReport);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CategoryReport categoryReport){
		categoryReportService.updateById(categoryReport);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] categoryId1s){
		categoryReportService.removeByIds(Arrays.asList(categoryId1s));

        return R.ok();
    }

}
