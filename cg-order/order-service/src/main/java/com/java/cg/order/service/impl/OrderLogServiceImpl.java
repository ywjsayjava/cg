package com.java.cg.order.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.order.dao.OrderLogDao;
import com.java.cg.order.entity.OrderLog;
import com.java.cg.order.service.OrderLogService;

@Service("orderLogService")
public class OrderLogServiceImpl extends ServiceImpl<OrderLogDao, OrderLog> implements OrderLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderLog> page = this.page(
                new Query<OrderLog>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}