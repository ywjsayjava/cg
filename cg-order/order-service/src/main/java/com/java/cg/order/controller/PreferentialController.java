package com.java.cg.order.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.cg.order.entity.Preferential;
import com.java.cg.order.service.PreferentialService;
import com.java.common.utils.PageUtils;
import com.java.common.utils.R;



/**
 * 
 *
 * @author jiangli
 * @since 2020-02-17 19:17:37
 */
@RestController
@RequestMapping("order/preferential")
public class PreferentialController {
    @Autowired
    private PreferentialService preferentialService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = preferentialService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
		Preferential preferential = preferentialService.getById(id);

        return R.ok().put("preferential", preferential);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Preferential preferential){
		preferentialService.save(preferential);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody Preferential preferential){
		preferentialService.updateById(preferential);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
		preferentialService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
