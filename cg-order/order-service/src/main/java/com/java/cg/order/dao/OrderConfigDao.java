package com.java.cg.order.dao;

import com.java.cg.order.entity.OrderConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-17 19:17:37
 */
@Mapper
public interface OrderConfigDao extends BaseMapper<OrderConfig> {
	
}
