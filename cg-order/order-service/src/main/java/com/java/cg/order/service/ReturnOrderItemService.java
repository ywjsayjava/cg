package com.java.cg.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageUtils;
import com.java.cg.order.entity.ReturnOrderItem;

import java.util.Map;

/**
 * 
 *
 * @author jiangli
 * @since 2020-02-17 19:17:36
 */
public interface ReturnOrderItemService extends IService<ReturnOrderItem> {

    PageUtils queryPage(Map<String, Object> params);
}

