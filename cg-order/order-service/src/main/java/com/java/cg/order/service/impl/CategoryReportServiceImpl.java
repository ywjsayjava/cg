package com.java.cg.order.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.order.dao.CategoryReportDao;
import com.java.cg.order.entity.CategoryReport;
import com.java.cg.order.service.CategoryReportService;

@Service("categoryReportService")
public class CategoryReportServiceImpl extends ServiceImpl<CategoryReportDao, CategoryReport> implements CategoryReportService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryReport> page = this.page(
                new Query<CategoryReport>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}