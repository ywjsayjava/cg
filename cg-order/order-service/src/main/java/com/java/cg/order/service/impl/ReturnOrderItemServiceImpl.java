package com.java.cg.order.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageUtils;
import com.java.common.utils.Query;

import com.java.cg.order.dao.ReturnOrderItemDao;
import com.java.cg.order.entity.ReturnOrderItem;
import com.java.cg.order.service.ReturnOrderItemService;

@Service("returnOrderItemService")
public class ReturnOrderItemServiceImpl extends ServiceImpl<ReturnOrderItemDao, ReturnOrderItem> implements ReturnOrderItemService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ReturnOrderItem> page = this.page(
                new Query<ReturnOrderItem>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

}