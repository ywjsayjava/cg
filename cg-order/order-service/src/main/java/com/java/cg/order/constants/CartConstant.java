package com.java.cg.order.constants;

/**
 * @author jiangli
 * @since 2020/1/31 16:25
 */
public interface CartConstant {

	/* redis key 前缀*/
	// 购物车前缀
	String CG_CART = "cg:cart:";
	// sku价格前缀
	String CG_SKU = "cg:sku:";

}
