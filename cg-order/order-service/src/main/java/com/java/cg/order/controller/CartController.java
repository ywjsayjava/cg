package com.java.cg.order.controller;

import com.java.cg.order.entity.OrderItem;
import com.java.cg.order.service.CartService;
import com.java.common.utils.R;
import com.java.common.utils.TokenDecode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author jiangli
 * @since 2020/2/17 19:33
 * 购物车
 */
@RestController
@RequestMapping("order/cart")
public class CartController {
	@Autowired
	private CartService cartService;

	/**
	 * 加入购物车
	 */
	@GetMapping("/add")
	public R addCart(Integer count, Long skuId) {
		String username = TokenDecode.getUserInfo().get("username");
		cartService.addCart(count,skuId,username);

		return R.ok();
	}


	/**
	 * 购物车列表
	 */
	@GetMapping("/list")
	public R list(){
		String username = TokenDecode.getUserInfo().get("username");
		List<OrderItem> list = cartService.list(username);
		return R.ok(list);
	}
}
