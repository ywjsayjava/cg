package com.java.cg.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-17 19:17:37
 */
@Data
@TableName("tb_order_log")
public class OrderLog implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private String id;
	/**
	 * 操作员
	 */
	private String operater;
	/**
	 * 操作时间
	 */
	private Date operateTime;
	/**
	 * 订单ID
	 */
	private String orderId;
	/**
	 * 订单状态,0未完成，1已完成，2，已退货
	 */
	private String orderStatus;
	/**
	 * 付款状态  0:未支付，1：已支付，2：支付失败
	 */
	private String payStatus;
	/**
	 * 发货状态
	 */
	private String consignStatus;
	/**
	 * 备注
	 */
	private String remarks;
	/**
	 * 支付金额
	 */
	private Integer money;
	/**
	 * 
	 */
	private String username;

}
