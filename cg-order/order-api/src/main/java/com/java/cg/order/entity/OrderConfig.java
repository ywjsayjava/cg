package com.java.cg.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-17 19:17:37
 */
@Data
@TableName("tb_order_config")
public class OrderConfig implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Integer id;
	/**
	 * 正常订单超时时间（分）
	 */
	private Integer orderTimeout;
	/**
	 * 秒杀订单超时时间（分）
	 */
	private Integer seckillTimeout;
	/**
	 * 自动收货（天）
	 */
	private Integer takeTimeout;
	/**
	 * 售后期限
	 */
	private Integer serviceTimeout;
	/**
	 * 自动五星好评
	 */
	private Integer commentTimeout;

}
