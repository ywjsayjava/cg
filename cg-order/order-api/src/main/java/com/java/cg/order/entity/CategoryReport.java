package com.java.cg.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-17 19:17:37
 */
@Data
@TableName("tb_category_report")
public class CategoryReport implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 1级分类
	 */
	@TableId
	private Integer categoryId1;
	/**
	 * 2级分类
	 */
	private Integer categoryId2;
	/**
	 * 3级分类
	 */
	private Integer categoryId3;
	/**
	 * 统计日期
	 */
	private Date countDate;
	/**
	 * 销售数量
	 */
	private Integer num;
	/**
	 * 销售额
	 */
	private Integer money;

}
