package com.java.cg.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-17 19:17:37
 */
@Data
@TableName("tb_preferential")
public class Preferential implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Integer id;
	/**
	 * 消费金额
	 */
	private Integer buyMoney;
	/**
	 * 优惠金额
	 */
	private Integer preMoney;
	/**
	 * 品类ID
	 */
	private Long categoryId;
	/**
	 * 活动开始日期
	 */
	private Date startTime;
	/**
	 * 活动截至日期
	 */
	private Date endTime;
	/**
	 * 状态
	 */
	private String state;
	/**
	 * 类型1不翻倍 2翻倍
	 */
	private String type;

}
