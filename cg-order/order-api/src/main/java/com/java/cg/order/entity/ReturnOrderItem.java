package com.java.cg.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author jiangli
 * @since 2020-02-17 19:17:36
 */
@Data
@TableName("tb_return_order_item")
public class ReturnOrderItem implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 分类ID
	 */
	private Long categoryId;
	/**
	 * SPU_ID
	 */
	private Long spuId;
	/**
	 * SKU_ID
	 */
	private Long skuId;
	/**
	 * 订单ID
	 */
	private Long orderId;
	/**
	 * 订单明细ID
	 */
	private Long orderItemId;
	/**
	 * 退货订单ID
	 */
	private Long returnOrderId;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 单价
	 */
	private Integer price;
	/**
	 * 数量
	 */
	private Integer num;
	/**
	 * 总金额
	 */
	private Integer money;
	/**
	 * 支付金额
	 */
	private Integer payMoney;
	/**
	 * 图片地址
	 */
	private String image;
	/**
	 * 重量
	 */
	private Integer weight;

}
